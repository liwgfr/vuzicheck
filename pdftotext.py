import requests
import os
from pathlib import Path


def reaek():
    filename = Path('metadata.pdf')
    url = 'https://www.rea.ru/ru/org/managements/priem/Documents/Normativ_Dokuments/2019/%D0%A1%D0%BF%D0%B8%D1%81%D0%BA%D0%B8_%D0%BF%D0%BE%D0%B4%D0%B0%D0%B2%D1%88%D0%B8%D1%85_2019/%D0%91%D0%B0%D0%BA%D0%B0%D0%BB%D0%B0%D0%B2%D1%80%D0%B8%D0%B0%D1%82%20%D0%B8%20%D1%81%D0%BF%D0%B5%D1%86%D0%B8%D0%B0%D0%BB%D0%B8%D1%82%D0%B5%D1%82/%D0%91%D1%8E%D0%B4%D0%B6%D0%B5%D1%82/%D0%B1%D0%B0%D0%BA%D0%B0%D0%BB%D0%B0%D0%B2%D1%80%D0%B8%D0%B0%D1%82/%D0%91%D0%90%D0%9A_%D0%91_%D0%9E%D0%9A_%D0%A4%D0%AD%D0%9F_%D0%93%D0%9C%D0%A3.pdf'

    response = requests.get(url)
    filename.write_bytes(response.content)

    os.system("pdf2txt.py -o metadata.txt metadata.pdf")


def reate():
    filename = Path('metadata.pdf')
    url = 'https://www.rea.ru/ru/org/managements/priem/Documents/Normativ_Dokuments/2019/%D0%A1%D0%BF%D0%B8%D1%81%D0%BA%D0%B8_%D0%BF%D0%BE%D0%B4%D0%B0%D0%B2%D1%88%D0%B8%D1%85_2019/%D0%91%D0%B0%D0%BA%D0%B0%D0%BB%D0%B0%D0%B2%D1%80%D0%B8%D0%B0%D1%82%20%D0%B8%20%D1%81%D0%BF%D0%B5%D1%86%D0%B8%D0%B0%D0%BB%D0%B8%D1%82%D0%B5%D1%82/%D0%91%D1%8E%D0%B4%D0%B6%D0%B5%D1%82/%D0%B1%D0%B0%D0%BA%D0%B0%D0%BB%D0%B0%D0%B2%D1%80%D0%B8%D0%B0%D1%82/%D0%91%D0%90%D0%9A_%D0%91_%D0%9E%D0%9A_%D0%A4%D0%AD%D0%A2%D0%A2_%D0%A2%D0%94.pdf'

    response = requests.get(url)
    filename.write_bytes(response.content)

    os.system("pdf2txt.py -o metadata.txt metadata.pdf")


def reaeb():
    filename = Path('metadata.pdf')
    url = 'https://www.rea.ru/ru/org/managements/priem/Documents/Normativ_Dokuments/2019/%D0%A1%D0%BF%D0%B8%D1%81%D0%BA%D0%B8_%D0%BF%D0%BE%D0%B4%D0%B0%D0%B2%D1%88%D0%B8%D1%85_2019/%D0%91%D0%B0%D0%BA%D0%B0%D0%BB%D0%B0%D0%B2%D1%80%D0%B8%D0%B0%D1%82%20%D0%B8%20%D1%81%D0%BF%D0%B5%D1%86%D0%B8%D0%B0%D0%BB%D0%B8%D1%82%D0%B5%D1%82/%D0%91%D1%8E%D0%B4%D0%B6%D0%B5%D1%82/%D1%81%D0%BF%D0%B5%D1%86%D0%B8%D0%B0%D0%BB%D0%B8%D1%82%D0%B5%D1%82/%D0%A1%D0%9F%D0%95%D0%A6_%D0%91_%D0%9E%D0%9A_%D0%A4%D0%AD%D0%9F_%D0%AD%D0%9A%D0%91%D0%95%D0%97.pdf'

    response = requests.get(url)
    filename.write_bytes(response.content)

    os.system("pdf2txt.py -o metadata.txt metadata.pdf")
